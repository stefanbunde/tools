#!/bin/bash

###############################################################################
# This shell script creates a backup for the listed directories. It is
# dependend on the connected hard drive which directories are chosen for the
# backup.
# The backup is stored in a tar file. To unpack this, run the following
# command:
#     tar -xf archive.tar -C /target/directory
###############################################################################

clear

BACKUP_DIRS_JUPITER='Desktop Dropbox edu etc finance it music photos unsort'
BACKUP_DIRS_SATURN='movies'


###############################################################################
# detect hard disk drive and choose backup path and backup dirs
###############################################################################

if   [ -d /media/aljechin/JUPITER ]; then
    BACKUP_PATH=/media/aljechin/JUPITER
    BACKUP_DIRS=$BACKUP_DIRS_JUPITER
elif [ -d /media/aljechin/SATURN ]; then
    BACKUP_PATH=/media/aljechin/SATURN
    BACKUP_DIRS=$BACKUP_DIRS_SATURN
else
    echo 'no hdd for backup found'
    exit
fi


###############################################################################
# make the backup
###############################################################################

BACKUP_START_TIME=$(date +%s)

for DIRECTORY in $BACKUP_DIRS
do
    echo "${DIRECTORY}"

    rsync -ahv --delete ${HOME}/${DIRECTORY} ${BACKUP_PATH}/
    continue

    #BACKUP_FILE=${BACKUP_PATH}/${DIRECTORY}_$(date +"%Y_%m_%d").tar
    #tar -cvf ${BACKUP_FILE} -C ~ $DIRECTORY
done

BACKUP_FINISH_TIME=$(date +%s)


###############################################################################
# log the used time for backup
###############################################################################
DIFF=$(( ${BACKUP_FINISH_TIME} - ${BACKUP_START_TIME} ))
HOURS=$(( ${DIFF} / 3600 ))
MINUTES=$(( ${DIFF}/60 - ${HOURS}*60 ))

echo 'duration: ' ${HOURS} ':' ${MINUTES}
