import datetime
import functools
import logging
import os
import re
import urllib.request
from socket import timeout

logging.basicConfig(level=logging.DEBUG)


DOWNLOAD_DIRECTORY = os.path.expanduser('~/Desktop/tagesschau/')

TAGESSCHAU_URL = 'https://www.tagesschau.de'

PATTERN_TAGESSCHAU_URL = re.compile(
    '<p class="dachzeile">\d{2}.\d{2}.\d{4} 20:00 Uhr</p>\n'
    '<h4 class="headline"><a href="(/multimedia/sendung/ts-\d+?.html)">tagesschau',
    re.S
)
PATTERN_TAGESTHEMEN_URL = re.compile('<a href="(/multimedia/sendung/tt-\d+?.html)">tagesthemen</a>')

VIDEO_QUALITY = 'webl'  # possible options are: webxl, webl, webm, webs


def compose(*functions):
    return functools.reduce(lambda f, g: lambda x: g(f(x)), functions, lambda x: x)


class Show:
    def __init__(self, _type, _date):
        self.type = _type
        self.date = _date

    @property
    def name(self):
        return f'{self.date}-{self.type}'

    @property
    def filename(self):
        return os.path.join(DOWNLOAD_DIRECTORY, f'{self.name}.mp4')


def main():
    compose(
        create_download_directory_if_it_does_not_exist,
        create_latest_downloaded_show_file_if_it_does_not_exists,
        get_latest_downloaded_show,
        compute_next_show,
        compute_not_downloaded_shows,
        get_detail_page_urls,
        get_download_urls,
        download_shows,
        update_latest_downloaded_show_file,
    )(DOWNLOAD_DIRECTORY)


def create_download_directory_if_it_does_not_exist(download_directory):
    if not os.path.exists(download_directory):
        logging.debug('create download directory: %s', download_directory)
        os.mkdir(download_directory)
    return download_directory


def create_latest_downloaded_show_file_if_it_does_not_exists(download_directory):
    filename = os.path.join(download_directory, 'latest')

    if not os.path.exists(filename):
        logging.debug('create new latest file: %s', filename)
        yesterday = datetime.date.today() - datetime.timedelta(days=1)
        with open(filename, 'w') as fh:
            fh.write(f'{yesterday}-tt')

    return filename


def get_latest_downloaded_show(filename):
    logging.info('get latest downloaded show from %s', filename)
    with open(filename, 'r') as fh:
        _date, _type = fh.read().strip().rsplit('-', 1)
    return Show(_type, datetime.datetime.strptime(_date, '%Y-%m-%d').date())


def compute_next_show(show):
    logging.debug('compute next show for %s', show.name)
    if show.type == 'ts':
        return Show('tt', show.date)
    else:
        return Show('ts', show.date + datetime.timedelta(days=1))


def compute_not_downloaded_shows(show):
    logging.info('compute not downloaded shows starting from %s', show.name)
    while show.date <= datetime.date.today():
        yield show
        show = compute_next_show(show)


def get_detail_page_urls(shows):
    for show in shows:
        logging.info('get detail page url for %s', show.name)
        html = compose(
            compute_video_archiv_url,
            get_html,
            retrieve_html_main_content
        )(show)

        pattern = PATTERN_TAGESSCHAU_URL if show.type == 'ts' else PATTERN_TAGESTHEMEN_URL
        match = pattern.search(html)
        if not match:
            logging.debug('detail page url not found')
            continue
        yield show, urllib.parse.urljoin(TAGESSCHAU_URL, match.group(1))


def compute_video_archiv_url(show):
    date = show.date.strftime("%Y%m%d")
    return f'{TAGESSCHAU_URL}/multimedia/video/videoarchiv2~_date-{date}.html'


def get_html(url):
    try:
        response = urllib.request.urlopen(url, timeout=5)
    except timeout:
        raise Exception(f'timeout: {url}')
    return response.read().decode('utf-8')


def retrieve_html_main_content(html):
    return re.search("<!-- START -->(.+?)<!-- section -->", html, re.S).group(1)


def get_download_urls(args):
    for show, detail_page_url in args:
        logging.info('get download url for %s', show.name)
        yield show, compose(get_html, search_download_url)(detail_page_url)


def search_download_url(html):
    download_urls = re.findall('https://download.media.tagesschau.de/.+?.web.{1,2}.h264.mp4', html)
    return next(filter(lambda url: VIDEO_QUALITY in url, download_urls))


def download_shows(args):
    for show, download_url in args:
        logging.info('start download for %s', show.name)
        urllib.request.urlretrieve(download_url, show.filename)
        yield show


def update_latest_downloaded_show_file(shows):
    for show in shows:
        with open(os.path.join(DOWNLOAD_DIRECTORY, 'latest'), 'w') as f:
            f.write(show.name)


if __name__ == "__main__":
    main()
