import re
import requests
from collections import defaultdict, namedtuple
from operator import attrgetter
from pprint import pprint


QUERY_STRINGS = [
    "@all removals",
    "@all rings",
    "@animaltoken",
    "@crafting",
    "@minerals",
    "@ore",
    "@potions",
    "Bar",
    "Cruisses",
    "Dye",
    "Essence", "Extract",
    "Fabric",
    "Greaves",
    "Hat", "Helm",
    "Iron",
    "Life",
    "Mail", "Mana", "Matter", "Medaillion",
    "Polished",
    "Rapier", "Robe", "Rose",
    "Scarf", "Steel", "Stone", "Sword",
    "Titanium",
    "Tunic",
]

Entry = namedtuple('Entry', 'price amount location'.split())

price_list = defaultdict(lambda: defaultdict(list))
lucrative_items = defaultdict(dict)


entries = set()

for query_string in QUERY_STRINGS:
    response = requests.get("http://greypal.el-fd.org/cgi-bin/querybot",
                            params={"action": "Both", "item": query_string})
    html = response.text

    result = re.findall("<TABLE>.+?</TABLE>", html, re.DOTALL)
    if not result:
        continue
    details_table, summaries_table = result

    entries.update(set(re.findall("<TR><TD>(.+?)<TD>(.+?)<TD>(.+?)<TD>(.+?)<TD>(.+?)<TD ALIGN.+?>(.+?)<TD ALIGN.+?>\s*(.+?)gc<TD><A.+?>(.+?)</A>", details_table)))

for _, _, _, location, action, amount, price, item in entries:
    amount = 1000000 if amount == "*" else int(amount)
    entry = Entry(float(price), amount, location)
    price_list[item][action].append(entry)

for item, data in price_list.items():
    if "Buying" not in data or "Selling" not in data:
        continue

    max_buying_price = max(map(attrgetter('price'), data["Buying"]))
    min_selling_price = min(map(attrgetter('price'), data["Selling"]))

    if max_buying_price <= min_selling_price:
        continue

    if max_buying_price - min_selling_price < 1:
        continue

    lucrative_items[item]["Buying"] = [entry for entry in price_list[item]["Buying"]
                                       if entry.price >= min_selling_price]
    lucrative_items[item]["Selling"] = [entry for entry in price_list[item]["Selling"]
                                        if entry.price <= max_buying_price]

pprint(dict(lucrative_items))
print(len(lucrative_items))

#print("{:>45} - {:>8.2f}".format(item_name, prices["sell_max"] - prices["buy_min"]))
