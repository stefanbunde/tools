echo "==============================================================="
echo "                    remove unused stuff"
echo "==============================================================="
echo
echo "clean up home directory"
rmdir ~/Documents/ ~/Music/ ~/Pictures/ ~/Public/ ~/Templates/ ~/Videos/
rm examples.desktop
echo
echo "remove deja dup - backup tool"
apt-get -qq purge deja-dup
echo
echo "remove games"
apt-get -qq purge aisleriot gnome-mahjongg gnome-mines gnome-sudoku
echo
echo "remove cheese - webcam tool"
apt-get -qq purge cheese
echo
echo "remove gnome contacts"
apt-get -qq purge gnome-contacts
echo
echo "remove libre office math"
apt-get -qq purge libreoffice-math
echo
echo
echo
echo "==============================================================="
echo "                    update system"
echo "==============================================================="
echo
echo "update"
apt-get -qq update
echo "upgrade"
apt-get -qq upgrade
echo "dist-upgrade"
apt-get -qq dist-upgrade
echo "autoremove"
apt-get -qq autoremove
echo
echo
echo
echo "==============================================================="
echo "                    install gnome desktop environment"
echo "==============================================================="
echo
echo "install gnome"
apt-get -qq install gnome-session-flashback
echo
echo "install compiz"
apt-get -qq install compizconfig-session-manager compiz-plugins
echo
echo
echo
echo "==============================================================="
echo "                    install linux tools"
echo "==============================================================="
echo
echo "install git, gitk and git gui"
apt-get -qq install git gitk git-gui
echo
echo "install htop - interactive process viewer"
apt-get -qq install htop
echo
echo "install synaptic package manager"
apt-get -qq install synaptic
echo
echo "install tree"
apt-get -qq install tree
echo
echo "install vim - command line text editor"
apt-get -qq install vim
echo "            - clone Vundle package manager for vim"
git clone https://github.com/gmarik/Vundle.vim.git ~/.vim/bundle/Vundle.vim
echo
echo
echo
echo "==============================================================="
echo "                    install software development tools"
echo "==============================================================="
echo
echo "install docker"
curl -fsSL https://get.docker.com/ | sh
usermod -aG docker `whoami`
echo
echo "install docker-compose"
curl -L "https://github.com/docker/compose/releases/download/1.11.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
curl -L https://raw.githubusercontent.com/docker/compose/$(docker-compose version --short)/contrib/completion/bash/docker-compose -o /etc/bash_completion.d/docker-compose
echo
echo "install sqlite"
apt-get -qq install sqlite
echo
echo "install sqlitebrowser"
apt-get -qq install sqlitebrowser
echo
echo "install virtualenv - tool for isolated python environments"
apt-get -qq install virtualenv virtualenvwrapper
echo
echo "install ipython"
apt-get -qq install ipython ipython3
echo
echo "install node.js 6"
curl -sL https://deb.nodesource.com/setup_6.x | -E bash -
apt-get -qq install nodejs
echo
echo
echo
echo "==============================================================="
echo "                    install other applications"
echo "==============================================================="
echo
echo "install chrome - google web browser"
apt-get -qq install chromium-browser
echo
echo "install gimp - GNU image manipulation programm"
apt-get -qq install gimp
echo
echo "install openjdk"
apt-get -qq install openjdk-7-jdk
echo
echo "install zim - desktop wiki editor"
apt-get -qq install zim
echo
echo
echo
echo "==============================================================="
echo "                    install latex"
echo "==============================================================="
echo
echo "instal latex"
apt-get -qq install texlive
apt-get -qq install texlive-doc-de texlive-doc-en
apt-get -qq install texlive-generic-extra
apt-get -qq install texlive-lang-german texlive-lang-spanish texlive-lang-portuguese
apt-get -qq install texlive-latex-extra
apt-get -qq install texlive-math-extra
apt-get -qq install latex-beamer
