import csv
from collections import defaultdict
from pprint import pprint


class Transaction(object):
    def __init__(self, csv_data):
        self.date = csv_data[0]
        self.year = int(self.date[-4:])
        self.booking = csv_data[2]
        self.name = csv_data[3]
        self.subject = csv_data[4]
        self.amount = float(csv_data[7].replace(".", "").replace(",", "."))

    def __str__(self):
        return "{} - {} - {} - {}".format(self.date, self.amount, self.name, self.subject)


class Invoice:
    def __init__(self, csv_data):
        self.member_id = csv_data[7]
        self.last_name = csv_data[9]
        self.first_name = csv_data[10]
        self.amount = float(csv_data[13].replace(",", "."))
        self.transactions = list()
        self.annotation = ""

    def __str__(self):
        return "{} - {}".format(self.member_id, self.amount)


def get_csv_file_rows(filename):
    with open(filename, encoding="ISO-8859-1") as fh:
        reader = csv.reader(fh, delimiter=";")
        for row in reader:
            yield row


def convert_csv_to_invoices(rows):
    skip_rows(rows, 1)
    for row in rows:
        yield Invoice(row)


def convert_csv_to_transactions(rows):
    skip_rows(rows, 7)
    for row in rows:
        if row[2] == "Gutschrift":
            yield Transaction(row)


def skip_rows(rows, count):
    for _ in range(count):
        next(rows)


def get_invoices():
    csv_rows = get_csv_file_rows("rechnungen.csv")
    return convert_csv_to_invoices(csv_rows)


def get_transactions():
    csv_rows = get_csv_file_rows("umsatz.csv")
    return convert_csv_to_transactions(csv_rows)


def main():
    invoices = list(get_invoices())
    transactions = get_transactions()
    update_invoice_data_with_transaction_data(invoices, transactions)

    with open("result.csv", "w") as fh:
        writer = csv.writer(fh, delimiter=";")
        writer.writerow("MitgliederID Vorname Nachname Rechnungsbetrag Ueberweisungbetrag Differenzbetrag Anmerkung".split())

        for invoice in invoices:
            invoice2019 = invoice.amount
            amount2019 = sum([t.amount for t in invoice.transactions])
            diff2019 =  amount2019 - invoice2019

            writer.writerow([invoice.member_id, invoice.first_name, invoice.last_name,
                invoice2019, amount2019, "{:.2f}".format(diff2019), invoice.annotation])


def update_invoice_data_with_transaction_data(invoices, transactions):
    for transaction in transactions:
        assign_transaction_and_update_invoice_data(invoices, transaction)


def assign_transaction_and_update_invoice_data(invoices, transaction):
    for idx in range(len(invoices)):
        invoice = invoices[idx]

        if invoice.member_id in transaction.subject:
            invoice.annotation = "Mitgliedsnummer im Betreff"
            invoice.transactions.append(transaction)
            return

        if all([invoice.first_name, invoice.last_name]):
            name = "{} {}".format(invoice.first_name, invoice.last_name)
            if (name.lower() == transaction.name.lower() or
                    name.lower().replace("ä", "ae").replace("ö", "oe").replace("ü", "ue").replace("ß", "ss") == transaction.name.lower() or
                    name.lower() in transaction.name.lower().replace("ß", "ss") or
                    name.lower().replace("ä", "a").replace("ö", "o").replace("ü", "u") == transaction.name.lower() or
                    "{} {}".format(invoice.first_name.split()[0], invoice.last_name).lower() == transaction.name.lower() or
                    "{} {}".format(invoice.first_name.split("-")[0], invoice.last_name).lower() == transaction.name.lower()):
                invoice.annotation = "Treffer auf 'Vorname Nachname' im Betreff"
                invoice.transactions.append(transaction)
                return

            if name.lower() in transaction.subject.lower():
                invoice.annotation = "'Vorname Nachname' im Betreff"
                invoice.transactions.append(transaction)
                return

            name = "{}, {}".format(invoice.last_name, invoice.first_name)
            if (name.lower() == transaction.name.lower() or
                    name.lower().replace("ä", "ae").replace("ö", "oe").replace("ü", "ue") == transaction.name.lower()):
                invoice.annotation = "Treffer auf 'Nachname, Vorname' im Betreff"
                invoice.transactions.append(transaction)
                return

            if invoice.first_name.lower() in transaction.name.lower() and invoice.last_name.lower() in transaction.name.lower():
                invoice.annotation = "Treffer Name im Kontoinhaber"
                invoice.transactions.append(transaction)
                return

    print(transaction)


if __name__ == "__main__":
    main()
